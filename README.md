# Editable

## Pour commencer
Clonez le projet.

Créez un dossier `editable` à la racine de votre server.

Dans ce dossier copiez-collez le dossier `data` depuis le clone.

Créez votre structure de fichiers

```shell
editable/
  |- data/
  |   |- person.json
  |   |- product.json
  |   |- points.json
  |- test/
  |   |- index.html # pour tester votre composant
  |   |- up.php # copier le fichier up ici pour les tests
  |- editable/
  |   |- editable.js # à ajouter à une page HTML pour que le comp. fonctionne
  |   |- editable.css # le css spécifique du composant.
  |- readme.md # la doc du composant
  
```

## fonctionnalités
>dans l'ordre de priorité (que vous n'êtes pas obligés de respecter...)
- afficher les données d'un tableau qui se trouve dans un fichier JSON
- rendre les données [éditable](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes#attr-contenteditable)
- Ajouter un filtre de donnée
- Ajouter un [tri](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) des données sur chaque colonne (ordre alphanumérique)
- ajouter un bouton pour ajouter une ligne / créer un nouvel objet
- ajouter un bouton pour sauvegarder les modifications
- BONUS : afficher les données en fonction du type (les images dans des balises img, les dates correctement etc.)

## wireframe
Pour vous donner une idée. En fonction des fonctionnalités que vous allez implémenter ça peut
devenir plus complexe.

N'hésitez pas à faire autrement.

![img/wf-exo-editable.png](img/wf-exo-editable.png)

## TIPS
Pour vous aider un peu, quelques pistes.

### pour créer le tableau
1. Récupérer l'url du JSON dans [l'attribut HTML](https://developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute)
2. Aller chercher les données avec l'[API FETCH](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
3. Prendre un élément du tableau n'importe lequel.
4. Créer une ligne pour l'entête du tableau
5. Parcourir les clefs sur cet élément  `for (let key in element) ...`
6. Pour chaque clef, créer une cellule dans la ligne et mettre la clef dedans
7. Puis parcourir le tableau en créant une ligne pour chaque objet...
8. ...

Pensez au fait que les clefs dans l'entête du tableau ne sont pas éditable !

### pour le html

Le plus simple, c'est de faire une grid css avec autant de colonne qu'il y a d'attribut dans
les objets.

### pour l'édition
Rien de difficile pour le rendre éditable...

Mais il va falloir mettre à jour les données dans le JS quand elles changent dans le HTML
#### Si vous utilisez des inputs
Rien de compliqué
#### Si vous utilisez des contenteditable
L'évènement à écouter sur les cellules s'appelle `input` ([un exemple](https://jsfiddle.net/ch6yn/2691/))

Pour récupérer le contenu `event.target.innerText`
#### Dans tous les cas
Le plus simple, c'est de mettre des attributs `data-id="???" data-attr="???"` 
sur chaque "cellule" puis de faire une fonction qui permet de retrouver l'objet grâce à
son `id` et de changer sa valeur pour la clef `attr`.

exemple
```js
function updateData(objectId, attribute){
    // trouver le bon objet dans le tableau de données
    // mettre à jour la valeur correspondant à attribute
}
```

### pour le filtre
Le plus simple, c'est de mettre un `input text`.

Quand sa valeur change on parcourt toutes les lignes du tableau (html)

Si `le innerText` (de la ligne) ne contient pas ce qu'on est en train de taper dans l'input
on met la ligne en `display: none`

Il faut penser à enlever le `display: none` des lignes qu'on garde !

### pour le tri
Il va falloir utiliser `sort()` sur le tableau des données.

Et il va falloir le faire dans une fonction qui prendra en paramètre la colonne sur laquelle
on veut trier (donc la clef / l'attribut des objets sur lequel on veut faire le tri).

Quelque chose qui ressemble (mais pas exactement) à 
```js
function trier(tableau, attr){
    return tableau.sort( (a, b) => /* comparer les attr de a et b */)
}
```

À chaque fois qu'on trie, il faut détruire le tableau HTML et le refaire avec les données 
mises à jour.

## pour ajouter une ligne
Créer un nouvel objet avec les bons attributs et des valeurs vide ou par défaut.

Puis le `push` dans le tableau.

Puis régénérer l'affichage

## pour la sauvegarde
Comme on n'a pas de "back" on ne peut pas vraiment sauvegarder.

Mais vous avez le fichier `up.php` qui retourne le JSON qu'on lui envoie.

Donc vous pouvez utiliser l'API fetch pour envoyer une requête à `up.php`. C'est un echo.
C'est-à-dire qu'il renvoie tout ce qu'on lui envoie.

Dans le `body` de la requête, il va falloir mettre les données en JSON.

Si tout va bien lorsqu'on clique sur le bouton le fichier up renvoie la même chose que ce
qu'on lui a envoyé, donc les données à jour en JSON.

> pour la config de la requête (fetch) il faut penser à `Content-Type: application/json`
> 
> vous pouvez l'envoyer en GET ou en POST ça revient au même.
 ## FETCH
- [Récupérer des data avec fetch](https://stackoverflow.com/questions/48841097/receive-and-process-json-using-fetch-api-in-javascript)
- [Une explication assez courte et complete](https://dmitripavlutin.com/fetch-with-json/)
- [Envoyer des data avec fetch](https://stackoverflow.com/questions/29775797/fetch-post-json-data)
  
exemple
```js
let monURL = 'data/points.json';
let monTab;

fetch(monURL)   // j'envoie ma requête
    .then(      // quand je reçois la réponse
        (response) => response.json() // je récupère le JSON de la réponse
    )
    .then(                            // quand c'est pret
        (tab) => {
            monTab = tab;           // je mets mes data dans ma variable
            creerMonTableauHtml();      // je lance ma fonction d'affichage
        }
    ).catch(
        (err) => {
            console.log(err);          // j'affiche les erreurs HTTP au cas où
        }
    );

```

## approches
Il y a deux approches pour créer le composant.

Si vous êtes très motivés, vous créez un ["vrai" composant](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
qui fonctionne comme ça :
```html
<ediTable source="/url/du/json"></ediTable>
<script src="/url/de/editable.js"></script>
```

Si vous voulez la méthode plus tranquille vous pouvez gérer ça en procédural/fonctionnel comme ça:
```html
<div id="monTableau" data-source="/url/du/json"></div>
<script src="/url/de/editable.js"></script>
<script >
    const monElement = document.getElementById('monTableau');
    createTable(monElement); // cette fonction doit être définie dans editable.js
</script>
```
