let data
let tableElem = document.createElement('table')


function createTable(elem){
// Récupérer l'url du JSON dans l'attribut HTML
let jsonURL = elem.getAttribute('data-source')
//console.log(jsonURL);

// Aller chercher les données avec l'API FETCH
fetch(jsonURL)
  .then((response) => response.json())
  .then(
       (lesdatadufichier) => {
              data = lesdatadufichier
              tableHeader()
              tableBody()
       });

elem.appendChild(tableElem)


}
function tableHeader(){
// Prendre un élément du tableau n'importe lequel.
let unElement = data[0]
// Créer une ligne pour l'entête du tableau
let tr = document.createElement('tr')
// Parcourir les clefs sur cet élément  for (let key in element) ...
for(let key in unElement){
     // Pour chaque clef, créer une cellule dans la ligne et mettre la clef dedans
     let th = document.createElement('th')
      th.id = "th-id"
      th.setAttribute('key', key)
    th.addEventListener('click', trier)
     th.innerText = key
     tr.appendChild(th) 
   }
   let thead = document.createElement('thead')
   thead.appendChild(tr)
   tableElem.appendChild(thead)
}



  function trier(e){
  let key = e.target.getAttribute('key')
  console.log(key)
  // sort sur les data sur la clef key
     data.sort(function(a, b) {
      return a[key] - b[key];
     });
    //console.log(data);
  // refaire tablebody()
    return tableBody()
}
     

function tableBody(){
      // effacer tbody avant de le refaire
      tableElem.querySelector('tbody')?.remove()
      // Puis parcourir le tableau en créant une ligne pour chaque objet...
      let tbody = document.createElement('tbody')
      for(let obj of data){
          let tr = document.createElement('tr')
          
          for(let key in obj){
              // pour chaque clef, créer une cellule dans la ligne et mettre la clef dedans
              let td = document.createElement('td')
              td.innerText = obj[key]
              tr.appendChild(td)
          }
          tbody.appendChild(tr)
      }
      tableElem.appendChild(tbody)
}

   function ajouter(){
    
    let tprice = document.getElementById("price").value;
    let tname = document.getElementById("name").value;
    let tavailable = document.getElementById("available").value;
    let ttag = document.getElementById("tag").value;
    // bouton = getElementById("bouton")
    // bouton.addEventListener("click", ajouter)
    
    
                 let recup ={
                     
                    name: tname,
                    price: tprice,
                    available: tavailable,
                    tag: ttag
                }

                data.push(recup)
                console.log(data)
                tableBody();
              }
   
             //FONCTION FILTRER: 
              
              let filter = document.querySelector("#idfilter");
              
              filter.addEventListener('input', e =>{
                let trs = document.querySelectorAll('tr');
                for (let tr of trs){
                    if (!tr.innerText.includes(filter.value)){
                        tr.style.display = "none" 
                    }else{ 
                        tr.style.display = ""
                    }
                }
                    
                
                
            })


    
   



